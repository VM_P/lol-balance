from dataclasses import dataclass
import json
import os
from typing import Optional

@dataclass
class ChampStats:
    region: str
    rank: str
    role: str
    name: str
    win_rate: float
    pick_rate: float
    ban_rate: float
    score: float = 0


g = open("./data/champions.json")
champion_data = json.load(g)
roles = ['top', 'jungle', 'mid', 'adc', 'supp']
ranks = ['iron', 'bronze', 'silver', 'gold', 'plat', 'diamond', 'master', 'grandmaster', 'challenger']
regions = ['na', 'euw', 'kr', 'br', 'eun', 'jp', 'lan', 'las', 'oce', 'ru', 'tr']


def Get_Data(mode, region, rank):
    path = "./data/"+mode+"/"+region+"/"+region+"_"+rank+".json"
    #print(region)
    if os.path.isfile(path) == False:
        #print("does not exist")
        return 0
    file = open(path)
    data = json.load(file)
    file.close()
    return data 

def Get_Name_From_Id(id):
    return champion_data[str(id)]['name']

def Form_List(data, rank, region):
    ret = []
    first_key = list(data.keys())[0]
    for role in roles:
        for k in data[first_key]['data']['win_rates'][role]:
            #print(region+', '+rank+', '+pos+', '+champion_data[k['champion_id']]['name'])
            #print(k['win_rate'])
            '''
            stats = {"champion_id": int(k['champion_id']),
                    "champion_name": champion_data[k['champion_id']]['name'],
                    "win_rate": k['win_rate'],
                    "pick_rate": k['pick_rate'],
                    "ban_rate": k['ban_rate'],
                    "counters": k['worst_against']['bad_against']
                    #"number of matches": k['matches']
                    }
            '''
            stats = ChampStats(region, rank, role, champion_data[k['champion_id']]['name'], k['win_rate'], k['pick_rate'], k['ban_rate'])
            ret.append(stats)
    return ret

# TODO: everything
def Ranked_Stats():
    ranked_list = {}
    # TODO: different Regions(11), different ranks (9), copy: champion_ranking
    # iterate through ranks
    list = []
    for region in regions:
        check = True
        for rank in ranks:
            data = Get_Data('ranked', region, rank)
            if data == 0: 
                check = False
                continue
            list.extend(Form_List(data, rank, region))
        if check == False: continue
    
    return list



g.close()