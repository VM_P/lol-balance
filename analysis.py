from collections import defaultdict
from dataclasses import dataclass
from itertools import groupby
import operator
from typing import List
import uggParser
import csv
#import ritoTest


regions = ['na', 'euw', 'kr', 'br', 'eun', 'jp', 'lan', 'las', 'oce', 'ru', 'tr']
ranks = ['iron', 'bronze', 'silver', 'gold', 'plat', 'diamond', 'master', 'grandmaster', 'challenger']

def is_champ_in_list(list:list(uggParser.champion_data), name):
    for champ in list:
        if champ.name == name:
            return True
    return False

# calculate a score indicating strength
def get_score(stats:uggParser.ChampStats):
    win_rate = stats.win_rate+1
    pick_rate = stats.pick_rate
    ban_rate = 0.2*(stats.ban_rate+1)
    return win_rate * pick_rate * ban_rate

def categorize(champ_list:List[uggParser.ChampStats]):
    dict = defaultdict(list)
    for champ in champ_list:
        dict[champ.region+champ.rank+champ.role].append(champ)
    return dict

# returns list(champion)
def get_tiers(champ_list:List[uggParser.ChampStats], top_tier=True):
    champ_list_copy = champ_list.copy()
    champ_list_copy2 = champ_list.copy()
    # filter out 
    for champ in champ_list_copy:
        if top_tier:
            if champ.win_rate < 52.0 or champ.ban_rate == 0 or champ.pick_rate < 0.5:
                champ_list_copy2.remove(champ)
        else:
            if champ.win_rate > 49.0 or champ.pick_rate < 0.5:
                champ_list_copy2.remove(champ)
    
    for champ in champ_list_copy2:
        champ.score = get_score(champ)

    if top_tier:
        champ_list_copy2.sort(key=operator.attrgetter('score'), reverse = True)
    else:
        champ_list_copy2.sort(key=operator.attrgetter('score'), reverse = False)
    
    # filter out options below/above average score
    cat = categorize(champ_list_copy2)
    cat_res = defaultdict(list)
    
    if top_tier:
        for custom_id in cat:
            total_score = 0.0          
            for champ in cat[custom_id]:
                total_score += champ.score                         
            average_score = total_score / len(cat[custom_id])
            #print(custom_id, average_score)
            for champ in cat[custom_id]:
                #print(champ.name, champ.score)
                if champ.score > average_score:
                    #print('removed', champ.name)
                    cat_res[custom_id].append(champ)
    else:
        for custom_id in cat:
            total_score = 0.0
            for champ in cat[custom_id]:
                total_score += champ.score
            avg_score = total_score / len(cat[custom_id])
            for champ in cat[custom_id]:
                if champ.score < avg_score:
                    cat_res[custom_id].append(champ)

    return cat_res

def tier_count(input:defaultdict(list)):
    res = {}
    for custom_id in input:
        for champ in input[custom_id]:
            if champ.role+','+champ.name in res:
                res[champ.role+','+champ.name] += 1
            else:
                res[champ.role+','+champ.name] = 1
            
    #res = sorted(res.items(), key=lambda kv: kv[1], reverse = True)
    return res

def subtract_lists(first, second):
    res = first.copy()
    for key in first:
        if key in second:
            res[key] -= second[key]
            if res[key] <= 0:
                del res[key]
    #res = sorted(res.items(), key=lambda kv: kv[1], reverse = True)
    return res


'''
def save_as_csv(champ_list:List[uggParser.ChampStats]):
    with open('output.csv', 'w', newline='') as file:
        writer = csv.writer(file, delimiter=';', quotechar='"', quoting=csv.QUOTE_MINIMAL)
        writer.writerow(['Region', 'Rank', 'Pos', 'Champ', 'Score'])
        dict = defaultdict(list)
        for champ in champ_list:
            dict[champ.region+champ.rank+champ.role].append(champ)

        for id in dict:
            for champ in dict[id]:
                writer.writerow([champ.region, champ.rank, champ.role, champ.name, champ.score, champ.win_rate, champ.pick_rate, champ.ban_rate])
'''

def save_stats_as_csv(cat:defaultdict(list), name):
    with open(name, 'w', newline='') as file:
        writer = csv.writer(file, delimiter=';', quotechar='"', quoting=csv.QUOTE_MINIMAL)
        writer.writerow(['Region', 'Rank', 'Role', 'Champ', 'Score', 'Win Rate', 'Pick Rate', 'Ban Rate'])

        for id in cat:
            for champ in cat[id]:
                writer.writerow([champ.region, champ.rank, champ.role, champ.name, champ.score, champ.win_rate, champ.pick_rate, champ.ban_rate])

def save_tiers_as_csv(counts, name):
    with open(name, 'w', newline='') as file:
        writer = csv.writer(file, delimiter=';', quotechar='"', quoting=csv.QUOTE_MINIMAL)
        writer.writerow(['Champion', 'Role', 'Votes'])

        for entry in counts:
            info = entry.split(',')
            writer.writerow([info[1], info[0], counts[entry]])


ranked_stats = uggParser.Ranked_Stats()
#print(ranked_kr_stats)

top_tiers = get_tiers(ranked_stats, True)
low_tiers = get_tiers(ranked_stats, False)

high_tier_counts = subtract_lists(tier_count(top_tiers), tier_count(low_tiers))
#high_tier_counts = tier_count(top_tiers)
low_tier_counts = subtract_lists(tier_count(low_tiers), tier_count(top_tiers))

save_stats_as_csv(top_tiers, './dest/above average.csv')
save_stats_as_csv(low_tiers, './dest/below average.csv')
save_tiers_as_csv(high_tier_counts, './dest/high_votes.csv')
save_tiers_as_csv(low_tier_counts, './dest/low_votes.csv')